<?php
	if(!isset($_POST['enviar'])){
		http_response_code(403);
		header("refresh:5; url=compu.php");
		die("Debe completar el formulario previamente.");
	}

	require_once("classes/computadora.class.php");

	$compu1= new Computadora(
		$_POST['fuente'],
		$_POST['motherboard'],
		$_POST['procesador'],
		$_POST['RAM'],
		$_POST['HDD'],
		$_POST['teclado'],
		$_POST['mouse']
	);

	$tjson=json_encode($compu1,JSON_PRETTY_PRINT);
	
	echo $tjson;

	//capturar infoemación en un archivo
	$archivo = __DIR__."/compus.json";
	if(!file_exists($archivo)){
		$file = fopen($archivo, "w");
		fwrite($file, "[\n");
	}
	else{
		$file = fopen($archivo, "c");
		fseek($file, -2, SEEK_END);
		fwrite($file,",\n");
	}

	fwrite($file, $tjson);
	fwrite($file, "\n]");
	fclose($file);

	echo "Computadora agregada exitosamente";
	
?>