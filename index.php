<?php
	if(isset($_POST['enviar'])){
		$nombre = $_POST['nom'];
		$paterno = $_POST['pat'];
		$materno = $_POST['mat'];
		$correo = $_POST['mail'];	
	}
	else{
		$nombre = "";
		$paterno = "";
		$materno = "";
		$correo = "";
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 1: Envío de Datos</title>
</head>
<body>
	<form method="POST" action="<?php $_SERVER['PHP_SELF']; ?>">
		<label>Nombre</label>
		<input type="text" name="nom">
		<label>Apellido paterno</label>
		<input type="text" name="pat">
		<label>Apellido materno</label>
		<input type="text" name="mat">
		<label>E-mail</label>
		<input type="email" name="mail">
		<input type="submit" name="enviar">
	</form>
	<h1>
		<?php
			echo "Su nombre es: " . $nombre . " " . $paterno . " " . $materno;
			echo "<br>";
			echo "Su correo es :".$correo;
		?>
	</h1>
</body>
</html>
