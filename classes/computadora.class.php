<?php
	class Computadora implements JsonSerializable{
		//atributos
		private $fuente ="";
		private $motherboard="";
		private $procesador="";
		private $ram="";
		private $disco_duro=0;
		private $teclado="";
		private $mouse="";

		//constructor
		function __construct($ft,$mb,$proc, $ram, $hd, $kb, $ms){
			$this->fuente=$ft;
			$this->motherboard=$mb;
			$this->procesador=$proc;
			$this->ram=$ram;
			$this->disco_duro=$hd;
			$this->teclado=$kb;
			$this->mouse=$ms;
		}

		//métodos
		function mostrar_compu($id){
			//var_dump($this);
			echo "<tr>";
			echo "<td>".$id."</td>";
			echo "<td>".$this->fuente."</td>";
			echo "<td>".$this->motherboard."</td>";
			echo "<td>".$this->procesador."</td>";
			echo "<td>".$this->ram."</td>";
			echo "<td>".$this->disco_duro."</td>";
			echo "<td>".$this->teclado."</td>";
			echo "<td>".$this->mouse."</td>";
			echo "</tr>";
		}

		function jsonSerialize(){
			return get_object_vars($this);
		}
	}
?>