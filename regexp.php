<?php

	/*

	Expresiones Regulares

	^ <-- indica el inicio de le búsqueda de una expresión regular
	$ <-- indica el final de la búsqueda de una expresión regular

	[] <-- agrupan una serie de caracteres
	() <-- agrupar diversos grupos de series
	{n} <-- repite la ocurrencia n número de veces

	* <-- 0 o más repeticiones de la cadena
	+ <-- 1 o más repeticiones de la cadena
	? <-- 0 o 1 repetición de la cadena

	| <-- incluir varias opciones de búsqueda

	*/

	function ValUser($user){
		//deberá contener un largo de 5 a 15 caracteres, además se aceptan - _ y .
		//maupalo
		if(preg_match("/^[-_.A-Za-z0-9]{5,15}$/",$user)){
			return true;
		}
		else{
			return false;
		}
	}

	function ValTexto($texto){
		//validar un texto no vacío y sin espacios
		if(preg_match("/^[a-zA-Z]+$/",$texto)){
			return true;
		}
		else{
			return false;
		}
	}

	function ValEntero($num){
		//validar un número entero de 4 a 8 dígitos entre 0 a 9
		if(preg_match("/^[0-9]{4,8}$/",$num)){
			return true;
		}
		else{
			return false;
		}
	}
	function ValFecha($fecha){
		//validar una fecha en formato dd/mm/aaaa desde el año 1900 hasta 2100
		//				01-09		10-29	30-31
		if(preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/(19[0-9]{2}|20[0-9]{2}|2100)$/",$fecha)){
			return true;
		}
		else{
			return false;
		}
	}

	function ValCorreo($correo){
		//Valida un correo
		//		Nombre de 8-30 caract.	@	Dominio 5-15 caract.	Extensiones .com .mx .org .net
		if(preg_match("/^([._a-zA-Z]{8,30})@([a-zA-Z]{5,15})[.](mx|com|org|net)$/",$correo)){
			return true;
		}
		else{
			return false;
		}
	}
	function ValDecimal($num){
		if(preg_match("/^([1-9]{4})[.]([1-9]{2})$/",$num)){
			return true;
		}
		else{
			return false;
		}
	}
	function ValUsu($usu){
		if(preg_match("/^([0-9]{3})[.]([a-zA-Z]{4})$/",$usu)){
			return true;
		}
		else{
			return false;
		}
	}
	function ValHora($hora){
		if(preg_match("/^(0[0-9]|1[0-9]|2[0-3])(:[0-5][0-9]){2}$/",$hora)){
			return true;
		}
		else{
			return false;
		}
	}



	$res = ValHora("23:59:59");
	if($res){
		echo "Sí es válido";
	}
	else
		echo "No es válido";

?>