<!DOCTYPE html>
<html>
<head>
	<title>Forumlario de computadoras</title>
</head>
<body>
<font face="Arial">
	<h1 align="center">FORMULARIO DE COMPUTADORAS</h1>
	<form method="POST" action="alta_compu.php">
		<fieldset>
		<legend>Configure su computadora</legend>
		<label>Fuente de Poder</label>
			<select name="fuente" required>
				<option value="400" selected>400w</option>
				<option value="500">500w</option>
				<option value="800">800w</option>
				<option value="1200">1200w</option>
			</select>
			<br><br>
		<label>Motherboard</label>
			<input type="text" name="motherboard" placeholder="Motherboard" required>
			<br><br>
		<label>Procesador</label>
			<input type="text" name="procesador" placeholder="Procesador" required>
			<br><br>
		<label>Memoria RAM</label>
			<select name="RAM" required>
				<option value="4" selected>4 GB</option>
				<option value="8">8 GB</option>
				<option value="16">16 GB</option>
				<option value="32">32 GB</option>
			</select>
			<br><br>
		<label>Disco Duro</label>
			<input type="number" name="HDD" placeholder="GB">
			<br><br>
		<fieldset>
			<legend>Periféricos</legend>
			<input type="checkbox" name="uoptica" value="uo">
			<label>Unidad Óptica</label>
			<br><br>
			<input type="checkbox" name="lector" value="lt">
			<label>Lector de Tarjetas</label>
			<br><br>
			<input type="checkbox" name="teclado" value="kb">
			<label>Teclado</label>
			<br><br>
			<input type="checkbox" name="mouse" value="ms">
			<label>Mouse</label>
		</fieldset>
		<br><br>
		<label>Comentarios</label>
			<textarea placeholder="Comentarios" name="comentarios"></textarea>
		<br><br>
		<input type="submit" name="enviar" value="Enviar">
		<input type="reset" name="rst" value="Borrar">
	</fieldset>
	</form>
</body>
</html>