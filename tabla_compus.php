<?php

	require_once("classes/computadora.class.php");
	
	$url=__DIR__."/compus.json";
	$compusList=array();

	//lectura de archivo json
	$archivo=file_get_contents($url);	

	//convertir el texto de json a arreglo de objetos
	$objetos = json_decode($archivo);

	foreach($objetos as $compu){
		$compuActual = new Computadora(
			$compu->fuente,
			$compu->motherboard,
			$compu->procesador,
			$compu->ram,
			$compu->disco_duro,
			$compu->teclado,
			$compu->mouse
		);

		array_push($compusList, $compuActual);
	}

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Tabla de Computadoras</title>
	</head>
	<body>
		<table width="500px" border="1" cellspacing="0" cellpadding="5">
			<tr>
				<th>ID</th>
				<th>Fuente</th>
				<th>Motherboard</th>
				<th>Procesador</th>
				<th>Memoria RAM</th>
				<th>Disco Duro</th>
				<th>Teclado</th>
				<th>Mouse</th>
			</tr>
			<?php
				$id =0 ;
				foreach($compusList as $compu){
					$id++;
					$compu->mostrar_compu($id);
				}
			?>
		</table>
	</body>
</html>